FROM python:2.7
FROM alpine:latest
WORKDIR /app
RUN apk add --no-cache perl net-snmp-perl net-snmp net-snmp-tools snmptt gcc g++ make libffi-dev musl
RUN cp /usr/bin/snmptt /usr/sbin/
RUN cp /usr/bin/snmptthandler /usr/sbin/
RUN chmod +x /usr/sbin/snmptt
CMD ["snmptrapd", "-O", "n"]

EXPOSE 162
